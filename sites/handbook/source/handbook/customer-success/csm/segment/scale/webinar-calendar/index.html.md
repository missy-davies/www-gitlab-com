---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars in the month of July.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## July 2023

### AMER Time Zone Webinars

#### Git Basics
##### July 7th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Are you new to Git? Attend this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_bYSLBmvZS1O_46rwKMUEDA#)

#### Intro to GitLab
##### July 11th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

New to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_d_Lgdzw9SMmX-LXLJkMSWA#)

#### Intro to CI/CD
##### July 18th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8__Dyj59RkWXvB5FtlNr5w#)

#### Advanced CI/CD
##### July 25th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_xEnLCQgCRzedFJzrkoXy9A#)

#### GitLab - The AI Powered DevSecOps Platform
##### July 26th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Join us to learn about the evolution of the Gitlab platform from DevOps to DevSecOps to the new AI-powered DevSecOps platform!

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_M5uLxfsURdWQxl1x1R1e_A#)

#### DevSecOps/Compliance
##### July 31st, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_cvGqU4gaSeWDvUJp2lN02g#)

### APAC Time Zone Webinars

#### Intro to GitLab
##### July 18th, 2023 at 2:00-3:00PM Singapore / 4:00-5:00PM Sydney / 11:30AM-12:30PM India / 1:00-2:00PM Indonesia

New to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_WzRIDMBpQJyV0lBLDGCEMw#)

#### Intro to CI/CD
##### July 20th, 2023 at 2:00-3:00PM Singapore / 4:00-5:00PM Sydney / 11:30AM-12:30PM India / 1:00-2:00PM Indonesia

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__YLOkJBNRtajcjW2_pSGgQ#)

#### Advanced CI/CD
##### July 25th, 2023 at 2:00-3:00PM Singapore / 4:00-5:00PM Sydney / 11:30AM-12:30PM India / 1:00-2:00PM Indonesia

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_RBS38KYzRi-BLQUpli4O0Q#)

#### DevSecOps/Compliance
##### July 31st, 2023 at 2:00-3:00PM Singapore / 4:00-5:00PM Sydney / 11:30AM-12:30PM India / 1:00-2:00PM Indonesia

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_ROUfTqaLS8m1QbK05kNtNg#)


### EMEA Time Zone Webinars

#### Intro to GitLab
##### July 11th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

New to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_yhmL6RLEQ0CxER1ucDLGrg#)

#### GitLab - The AI Powered DevSecOps Platform
##### July 18th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

Join us to learn about the evolution of the Gitlab platform from DevOps to DevSecOps to the new AI-powered DevSecOps platform!

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Tmo20HMtQG-X81DY3j8w8Q#)

#### Intro to CI/CD
##### July 20th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_OztFR_JMTC65F7DFj7ibPA#)

#### Advanced CI/CD
##### July 25th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST
Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_R8DdvEmuTxaMx6gGLGX7Rw#)

#### DevSecOps/Compliance
##### July 31st, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_4W3FFjr4Q92eYmLNvU_HJw#)
    
Check back later for more webinars! 
