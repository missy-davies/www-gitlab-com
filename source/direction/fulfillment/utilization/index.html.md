---
layout: markdown_page
title: "Category Direction - Utilization"
description: "The Utilization strategy page belongs to the Utilization group of the Fulfillment stage"
---

- TOC
{:toc}


## Fulfillment: Utilization Overview

Utilization data is the key that unlocks efficiency and optimization of the sales cycle. When a sales team member or a user themselves have access to consumables usage data, they can make decisions on their own which greatly increase probability of sale and delivers an amazing user experience. The Utilization group endeavors to capture and deliver usage data to internal team members, prospects, and customers so that they can make the best decision for their business needs. The Utilization group builds products that support the following phases of the sales and renewal cycle:

<details>
<summary markdown="span"><b>Utilization Sales & Renewal Cycle: Details </b></summary>

<br><br>Pre-sale: 

The user is making use of our Free product or on a Trial. During this phase, we want to provide sufficient information about current usage and what the user can expect to order when they are ready to make a purchase.

The minimum order the user needs to make to cover their current usage should be very clear, and we should provide clarity around what a paid subscription looks like - in other words, how will they be billed for their usage. There should be no surprises about when users are billable, how units of compute are billed and which storage types are consuming the storage allocation.

<br><br>Post-sale & purchase management: 

Once a product has been purchased, the user should have visibility into the usage of the purchase as well as have tools to manage the usage of that purchase. For example, when buying a subscription with storage, the user will want to know which file types are taking up how much storage and how many unallocated storage is still available to use before additional costs are incurred. When buying compute or storage, the user should be informed about consumption of the compute and storage relative to what they have available, in order to take action to purchase more, reduce consumption or consider the next product tier (Ultimate).

Further to having visibility into the usage, the user should also have controls and tools to manage the usage. The management actions can include: setting restrictions on projects for the compute and storage they can consume and requiring approvals for users who can take up a seat.

<br><br>Pre-renewal: 

For this phase, historical data becomes important as it informs the user about what their renewal order should look like. It can also give a view of their trajectory over the last 12 months so that they can renew with their anticipated future growth in mind.

The accuracy and reliability of our data, presented in a clear and understandable manner, will allow for a frictionless order process as conversations about the renewal is supported and informed by historical usage information.
</details>

## Vision

A sophisticated Utilization product would provide predictability - either our own systems monitoring and suggesting purchasing actions based on usage behaviour, or empowering our sales team to have a conversation about growth with detailed usage data as a foundation for that conversation.

## Feature Overview and Maturity

_What features is the Utilization group responsible for and how mature are they?_

**Legend**:

- 🙂 **Minimal**: Available and works for a small number of use cases. Some transparency for internal teams.
- 😊 **Viable**: Available and works for majority of use cases. Some transparency for internal teams.
- 😁 **Complete**: Fully functional for all eligible use cases. Full transparency for internal teams.
- 😍 **Lovable**: Glowing review from external and internal users.

| Feature | Maturity | Description | 
|---------|:--------:|-------------|
| Storage Usage Visibility | 🙂 Minimal | Customers understand how much storage they are using and what projects/file types are contributing to that usage. |
| Storage Quota | 🙂 Minimal | Customers understand if they are within the storage limits theshold and how to take action (remove, add more, set storage limits)  |
| Compute units Usage Visibility | 😊 Viable | Customers understand how many units of compute they are using |
| Quota of compute units | 😊 Viable | Customers understand if they are running out of units of compute and how to buy more  |

## 1-year Plan

_Where are we focused over the next 12 months to make meaningful steps towards achieving our vision and increasing feature maturity?_

### 1-year Vision

In a year from now, we hope to have:
1. Provided users with [MVC storage visibility](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1769)
2. Supported [free storage enforcement](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/1770)

### Roadmap

- [FY'24 Q2 Utilization OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Autilization&label_name%5B%5D=FY24-Q2&first_page_size=20) (Not Public)

### Prior Work
- [FY'24 Q1 Fulfillment OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/695) (Not Public)

## Additional Information

### Q&A

| Question | Answer | 
|---------|-------------|
| What type of customers does Utilization serve? | - Self-service customers <br>- Sales assisted customers <br>- Channel Partners and their customers  |
| What customer personas are Utilization solving for? | Our customers fit the [buyer persona](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/) and may play a different role in the decision-making and purchasing process depending on their company size and their role.   |
| What customer segment are Utilization focused on? | - For SMB and mid-market company: [The application development manager](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/#app-dev-avery) needs to have visibility into usage across their teams and be able to control usage in a way that fits their company preferences/processes/budget. <br> - For large or enterprise company: [The release and change management director](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/#release-rory) is concerned with acurate billing and being able to make purchasing decisions based on usage information. |
| What internal teams does Utilization serve? | - [Support](https://about.gitlab.com/handbook/support/) <br>- [Customer Success](https://about.gitlab.com/handbook/customer-success/) <br>- [Sales](https://about.gitlab.com/handbook/sales/)  |
|What is the Utilization group focused on for the next year?| We will be primarily focused on delivering functionality related to storage visibility and enforcement.|
|What is the Utilization group not responsible for?|The Utilization Group relies on calculations provided by other teams as part of building the right reporting and visualization for customers and admins. However, Utilization is not responsible for the collection or raw calculations of this underlying data. Specifically, Utilization relies on Enablement teams to provide accurate data around things such as: <br>1. Project-level storage calculations (git repo + git LFS)<br>2. Namespace storage calculation: git repo, LFS, artifacts, container registry, etc. <br> 3. Units of compute |

### Key Links

- [Internal Handbook Page - Storage](https://internal-handbook.gitlab.io/handbook/product/fulfillment/storage-limits-enforcement/)
- Roadmap
    - Find our roadmap [here](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name[]=Fulfillment+Roadmap&label_name[]=group::utilization&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false).
    - We also have Fulfillment [FY'24 Plans and Prioritization](https://gitlab.com/gitlab-com/Product/-/issues/5291) (Not Public), that internal team members can reference to track all planned initiatives by theme.



